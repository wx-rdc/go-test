# Test

The fantastic TEST framework for Gin, aims to be developer friendly.

## Overview

* Full-Featured testing
* Every feature comes with tests
* Developer Friendly

## Getting Started

Add folder `t` to your project root.

Use `init.go` to add initialization actions just like in `main.go`

```
package t

import (
	"io"

	"github.com/gin-gonic/gin"
)

func testInit() (*gin.Engine, error) {
	gin.DefaultWriter = io.Discard

	route := routers.InitRouter()
	return route, nil
}
```

Add new TestCase file, such as `dummy_test.go`

```
package t

import (
	"testing"

	"gitee.com/wx-rdc/go-test"
)

func TestDummy(t *testing.T) {
	var caseList = []test.TestCase{
		{
			Method:      "GET",
			Url:         "/api/v1/kms/rsa/generateKeyPair",
			ContentType: "application/x-www-form-urlencoded",
			Param:       "",
			Code:        200,
			Desc:        "生成RSA密钥对",
			ShowBody:    true,
			ErrMsg:      "",
			Ext1:        nil,
			Ext2:        nil,
		},
	}
	
	test.RegisterInitCallback(testInit)
	test.Exec(t, caseList)
}
```

Now, you can run test like usual
```
go test -v ./t
```

## Authorization

This framework support `Bear TOKEN` authorization header, add token to cast

```
{
	Method:      "POST",
	Url:         "/auth",
	ContentType: "application/json",
	Param:       `{"username": "admin", "password": "123456"}`,
	Code:        200,
	Desc:        "登录",
	ShowBody:    true,
	ErrMsg:      "",
	Ext1:        nil,
	Ext2:        nil,
},
{
	Method:        "GET",
	Url:           "/api/v1/deviceList",
	ContentType:   "application/json",
	Authorization: `$token`,
	Param:         "",
	Code:          200,
	Desc:          "获取设备列表",
	ShowBody:      true,
	ErrMsg:        "",
	Ext1:          nil,
	Ext2:          nil,
}
```

## License

© Edward.Zhu, 2023~time.Now

Released under the [MIT License](https://gitee.com/wx-rdc/go-test/blob/master/License)
