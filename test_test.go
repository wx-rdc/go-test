package test

import (
	"encoding/json"
	"io"
	"testing"

	"github.com/gin-gonic/gin"
)

func testInit() (*gin.Engine, error) {
	gin.DefaultWriter = io.Discard

	return gin.New(), nil
}

func TestSm4Cipher(t *testing.T) {

	params := `{"handle":29,"keyPair":{
		"sign_pkey":"AfEpZwu31i5TcXkWFi0mjkc8YjxP8NWwISCrzS6oNbiK",
		"sign_skey":"p4XCh3zm9r5ae698ZMkx5GHbnwIe4wb0IJyiiqnpGR0KHqKggV5u1kHJdIHfjqFJiMkR3G+PFO/c5Eoy",
		"enc_pkey":"ALnPoIgKdFnjqauiIBNIXBfIV/XPOblnbNQJ/55a2dV5",
		"enc_skey":"bLMFeGkOeoV7FguPG/3skz05XbsHis3dBjVPkbBYihntTFj36pUDfedGpkBMlHFGzlRcwrHv+p1kDWgu",
		"ids": ["abc", "def"],
		"arr": [{"id": 1234, "name": "pdd"}, {"id": 3333, "name": "jd"}]
		}}`
	vars := make(map[string]interface{})
	json.Unmarshal([]byte(params), &vars)
	var caseList = []TestCase{
		{
			Method:      "GET",
			Url:         "/api/v1/kms/rsa/generateKeyPair",
			ContentType: "application/x-www-form-urlencoded",
			Param:       `{"name": ["$$rand"]}`,
			Code:        200,
			Desc:        "生成RSA密钥对",
			ShowBody:    true,
			ErrMsg:      "",
			Ext1:        nil,
			Ext2:        nil,
		},
		{
			Method:      "POST",
			Url:         "/api/v1/kms/rsa/importKeyPairWithKEK",
			ContentType: "application/x-www-form-urlencoded",
			Param: `{"kek_index": "$id", "ipx_index": 21, 
			"sign_pkey":"$keyPair.sign_pkey",	
			"sign_skey":"$keyPair.sign_skey", 
			"enc_pkey":"$keyPair.enc_pkey", 
			"enc_skey":"$keyPair.enc_skey"}`,
			Code:     200,
			Desc:     "导入非对称密钥到指定位置并用KEK解密私钥",
			ShowBody: true,
			ErrMsg:   "",
			Ext1:     nil,
			Ext2:     nil,
		},
		{
			Method:      "POST",
			Url:         "/api/v1/kms/rsa/generateKeyPair",
			ContentType: "application/json",
			Param:       `{"id": "$keyPair.ids#0", "name": "$keyPair.arr#1.name"}`,
			Code:        200,
			Desc:        "测试数组A",
			ShowBody:    true,
			ErrMsg:      "",
			Ext1:        nil,
			Ext2:        nil,
		},
	}

	RegisterInitCallback(testInit)
	ExecWithVars(t, caseList, vars)
}
