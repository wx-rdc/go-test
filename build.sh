#!/bin/bash

usage() {
    echo -e "\nUsage: $0 [tag]"
    echo
    echo "For example:"
    echo -e "\n\t$0 v0.1.5\n"
}

if [ -z $1 ]; then
    usage
    exit 0
fi

git push

git tag $1

git push origin $1